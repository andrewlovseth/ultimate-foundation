<?php

/*

	Template Name: Events

*/

get_header(); ?>

	<section id="hero">
		<div class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
			<div class="wrapper">

				<div class="info">
					<h1>
						<span><?php the_field('hero_headline'); ?></span>
					</h1>
				</div>

			</div>
		</div>
	</section>

	<section id="main">
			
		<section id="then-now">
			<div class="wrapper">

				<div class="then-now-wrapper">
					<div id="then">
						<h3><?php the_field('from_then_headline'); ?></h3>
						<img src="<?php $image = get_field('from_then_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div id="now">
						<h3><?php the_field('to_now_headline'); ?></h3>
						<img src="<?php $image = get_field('to_now_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

			</div>
		</section>
			
		<section id="details">
			<div class="wrapper">

				<?php the_field('details'); ?>

			</div>
		</section>

	</section>


<?php get_footer(); ?>