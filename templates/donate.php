<?php

/*

	Template Name: Donate

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
						
		<section id="donate-btns">
			<div class="wrapper">

				<div class="donate-btns-wrapper">

					<?php if(have_rows('donate_buttons')): while(have_rows('donate_buttons')): the_row(); ?>
					 
					    <div class="donate-btn">
					        <h3><?php the_sub_field('headline'); ?></h3>
					        <a href="<?php the_sub_field('button_link'); ?>" class="btn" rel="external"><?php the_sub_field('button_text'); ?></a>

							<?php if(get_sub_field('description')): ?>
								<div class="description">
									<?php the_sub_field('description'); ?>
								</div>						
							<?php endif; ?>
					    </div>

					<?php endwhile; endif; ?>

				</div>

				<div class="copy">
					<?php the_field('content'); ?>
				</div>

			</div>
		</section>

	</section>

<?php get_footer(); ?>