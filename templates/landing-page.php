<?php 

/*

	Template Name: Landing Page

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section id="main">
			
		<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
		
			<?php get_template_part('templates/landing-page/left-image'); ?>

			<?php get_template_part('templates/landing-page/right-image'); ?>

			<?php get_template_part('templates/landing-page/left-multiple-images'); ?>

			<?php get_template_part('templates/landing-page/donate'); ?>

			<?php get_template_part('templates/landing-page/get-involved'); ?>

			<?php get_template_part('templates/landing-page/section-headline'); ?>

			<?php get_template_part('templates/landing-page/quote'); ?>

			<?php get_template_part('templates/landing-page/staff'); ?>

        <?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>