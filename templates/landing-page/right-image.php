<?php if( get_row_layout() == 'right_image' ): ?>
    
    <section class="right-image two-col" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>">
        <div class="wrapper">
            <?php get_template_part('template-parts/global/info'); ?>

            <?php get_template_part('template-parts/global/image'); ?>
        </div>
    </section>
    
<?php endif; ?>