<?php if( get_row_layout() == 'left_image' ): ?>
    
    <section class="left-image two-col" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>">
        <div class="wrapper">
            <?php get_template_part('template-parts/global/image'); ?>

            <?php get_template_part('template-parts/global/info'); ?>
        </div>
    </section>
    
<?php endif; ?>