<?php if( get_row_layout() == 'section_headline' ): ?>
    
    <section class="headline">
        <div class="wrapper">
        
            <?php get_template_part('template-parts/global/headline'); ?>
            
        </div>
    </section>
    
<?php endif; ?>