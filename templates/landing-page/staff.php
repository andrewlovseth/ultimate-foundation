<?php if( get_row_layout() == 'staff' ): ?>
    
    <section class="staff">
        <div class="wrapper">

            <div class="headline staff__headline">
                <h2><?php the_sub_field('headline'); ?></h2>
            </div>
        
            <div class="staff__grid">

                <?php if(have_rows('staff')): while(have_rows('staff')): the_row(); ?>

                    <?php
                        $photo = get_sub_field('photo');
                        $name = get_sub_field('name');
                        $title = get_sub_field('title');
                        $email = get_sub_field('email');
                        $phone = get_sub_field('phone');
                    ?>
                
                    <div class="staff__member">
                        <?php if($photo): ?>
                            <div class="staff__photo">
                                <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
                            </div>
                        <?php endif; ?>

                        <div class="staff__info">
                            <?php if($name): ?>
                                <h3 class="staff__name"><?php echo $name; ?></h3>
                            <?php endif; ?>

                            <?php if($title): ?>
                                <h4 class="staff__title"><?php echo $title; ?></h4>
                            <?php endif; ?>

                            <?php if($email): ?>
                                <p class="staff__email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
                            <?php endif; ?>

                            <?php if($email): ?>
                                <p class="staff__phone"><?php echo $phone; ?></p>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php endwhile; endif; ?>


            </div>
                   
        </div>
    </section>
    
<?php endif; ?>
