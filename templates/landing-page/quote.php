<?php if( get_row_layout() == 'quote' ): ?>
    
    <section class="quote">
        <div class="wrapper">
        
            <div class="info">
                <?php the_sub_field('copy'); ?>
            </div>
                   
        </div>
    </section>
    
<?php endif; ?>
