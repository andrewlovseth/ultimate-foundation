<?php if( get_row_layout() == 'donate' ): ?>
	
	<section class="donate">
		<div class="wrapper">
		
			<div class="info">
				<h3><?php the_sub_field('headline'); ?></h3>
				<?php the_sub_field('copy'); ?>

				<div class="donate-btn">
					<a href="<?php echo site_url('/donate/'); ?>" class="btn">Donate Now</a>
				</div>
			</div>
			
		</div>
	</section>
	
<?php endif; ?>