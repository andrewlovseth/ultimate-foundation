<?php if( get_row_layout() == 'left_multiple_images' ): ?>
    
    <section class="left-multiple-images two-col" id="<?php echo sanitize_title_with_dashes(get_sub_field('headline')); ?>">
        <div class="wrapper">
            <?php get_template_part('template-parts/global/gallery'); ?>
            
            <?php get_template_part('template-parts/global/info'); ?>
        </div>
    </section>
    
<?php endif; ?>