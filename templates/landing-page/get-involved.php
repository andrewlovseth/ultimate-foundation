<?php if( get_row_layout() == 'get_involved' ): ?>
    
    <section class="get-involved">
        <div class="wrapper">
        
            <div class="info">
                <h3><?php the_sub_field('headline'); ?></h3>
                <?php the_sub_field('copy'); ?>
            </div>

        </div>
    </section>
    
<?php endif; ?>