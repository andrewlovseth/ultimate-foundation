<?php get_header(); ?>


	<section id="hero">
		<div class="hero-image" style="background-image: url(<?php $image = get_field('blog_hero_image', 'options'); echo $image['url']; ?>);">
			<div class="wrapper">

				<div class="info">
					<h1>
						<span><?php the_field('blog_hero_headline', 'options'); ?></span>
					</h1>
				</div>

			</div>
		</div>
	</section>


	<section id="posts">

		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<article>
					<div class="photo">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</div>

					<div class="info">
						<h4><?php the_time('F j, Y') ; ?></h4>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php the_field('excerpt'); ?>
						<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>				
					</div>
				</article>

			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>