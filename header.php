<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700|Open+Sans+Condensed:300,900|Montserrat:400,700|Rajdhani:700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<nav>
				<div class="ctas">
					<?php if(have_rows('ctas', 'options')): while(have_rows('ctas', 'options')): the_row(); ?>
					 
						 <?php if(get_sub_field('hide') != true): ?>
						    <a href="<?php the_sub_field('link'); ?>" class="btn"><?php the_sub_field('label'); ?></a>
						 <?php endif; ?>

					<?php endwhile; endif; ?>
				</div>

				<a href="#" id="toggle" class="no-translate">
					<div class="patty"></div>
				</a>

				<section class="desktop nav-links">
					<div class="nav-links-wrapper">
						<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
						 
						    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

						<?php endwhile; endif; ?>
					</div>
				</section>

			</nav>

		</div>
	</header>

	<section class="mobile nav-links">
		<div class="wrapper">
	
			<div class="nav-links-wrapper">
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

				<?php endwhile; endif; ?>
			</div>

			<div class="ctas">
				<?php if(have_rows('ctas', 'options')): while(have_rows('ctas', 'options')): the_row(); ?>
				 
					 <div class="link">
					    <a href="<?php the_sub_field('link'); ?>" class="btn"><?php the_sub_field('label'); ?></a>
					 </div>

				<?php endwhile; endif; ?>
			</div>

			
		</div>
	</section>


