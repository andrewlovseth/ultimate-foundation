	<footer>
		<div class="wrapper">
			<p class="copyright">
				<?php the_field('copyright', 'options'); ?>
			</p>

		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110477478-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-110477478-1');
	</script>

</body>
</html>