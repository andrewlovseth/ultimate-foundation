$(document).ready(function() {
	
	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('.nav-links.mobile').slideToggle(300);
		return false;
	});

});