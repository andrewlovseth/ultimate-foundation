<div class="info">
	<?php if(get_sub_field('headline')): ?>
		<h3><?php the_sub_field('headline'); ?></h3>
	<?php endif; ?>

	<?php the_sub_field('copy'); ?>
</div>