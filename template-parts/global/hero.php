<section id="hero">
    <div class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
        <div class="wrapper">

            <div class="info">
                <h1>
                    <span><?php the_field('hero_headline'); ?></span>
                </h1>
            </div>

        </div>
    </div>
</section>