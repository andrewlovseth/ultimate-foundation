<div class="image">
	<div class="image-wrapper">
		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
</div>