<div class="image gallery">
	<?php $images = get_sub_field('image'); if( $images ): ?>
		<?php foreach( $images as $image ): ?>
			<div class="image-wrapper">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>